<?php

// +----------------------------------------------------------------------
// | HisiPHP框架[基于ThinkPHP5.1开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2018 http://www.HisiPHP.com
// +----------------------------------------------------------------------
// | HisiPHP提供个人非商业用途免费使用，商业需授权。
// +----------------------------------------------------------------------
// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
// +----------------------------------------------------------------------

// [ 后台入口文件 ]
namespace think;

header('Content-Type:text/html;charset=utf-8');

ini_set('memory_limit', '1024M');

//印度加尔各答时区
date_default_timezone_set('Asia/Calcutta');

// 定义应用目录
define('APP_PATH', __DIR__ . '/application/');

// 定义入口为admin
define('ENTRANCE', 'admin');

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

// 检查是否安装
if (!is_file('./../install.lock')) {
    header('location: /');
} else {
    Container::get('app')->run()->send();
}



//// +----------------------------------------------------------------------
//// | HisiPHP框架[基于ThinkPHP5.1开发]
//// +----------------------------------------------------------------------
//// | Copyright (c) 2016-2018 http://www.HisiPHP.com
//// +----------------------------------------------------------------------
//// | HisiPHP提供个人非商业用途免费使用，商业需授权。
//// +----------------------------------------------------------------------
//// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
//// +----------------------------------------------------------------------
//
//// [ 应用入口文件 ]
//namespace think;
//
//header('Content-Type:text/html;charset=utf-8');
//
//ini_set('memory_limit', '256M');
//
//date_default_timezone_set('Indian/Antananarivo');
//
//// 检测PHP环境
//if (version_compare(PHP_VERSION, '5.6.0', '<')) die('PHP版本过低，最少需要PHP5.6，请升级PHP版本！');
//
//// 定义应用目录
//define('APP_PATH', __DIR__ . '/application/');
//
//// 加载基础文件
//require __DIR__ . '/../thinkphp/base.php';
//
//// 检查是否安装
//if (!is_file('./../install.lock')) {
//
//    define('INSTALL_ENTRANCE', true);
//    Container::get('app')->bind('install')->run()->send();
//
//} else {
//
//    Container::get('app')->run()->send();
//
//}