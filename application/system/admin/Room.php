<?php

namespace app\system\admin;

use app\system\model\RoomConfig;
use app\system\model\RoomsConfig;
use app\system\model\RoomsDuoSquad;
use app\system\model\RoomsPlayer;
use app\system\model\RoomsResult;
use app\system\model\RoomsSoloTop;
use app\system\model\RoomsTeam;
use app\system\model\SystemConfig as ConfigModel;
use app\system\model\Room as RoomModel;
use app\system\model\RoomsConfig as RoomsConfigModel;
use hisi\Http;
use think\facade\Log;
use think\facade\Env;

class Room extends Admin
{
    protected $hisiTable = 'SystemConfig';

    protected function initialize()
    {
        parent::initialize();
        !Env::get('app_debug') && $this->error('非开发模式禁止访问！');
    }

    public function index($group = 'base', $id = 0)
    {
        if ($this->request->isAjax()) {
            $where = $data = [];
            $page = $this->request->param('page/d', 1);
            $limit = $this->request->param('limit/d', 15);
            $id = $this->request->param('id', 0);
            $game_room_id = $this->request->param('game_room_id', 0);
            if ($id) {
                $where['id'] = $id;
            }
            if ($game_room_id) {
                $where['game_room_id'] = $game_room_id;
            }
            $data['data'] = RoomModel::getList($where, $page, $limit);
            $data['count'] = RoomModel::where($where)->count('id');
            $data['code'] = 0;
            return json($data);
        }

        $tabData = [];
        foreach (config('hs_system.room_group') as $key => $value) {
            $arr = [];
            $arr['title'] = $value;
            $arr['url'] = '?group=' . $key;
            $tabData['menu'][] = $arr;
        }

        $tabData['current'] = url('?group=' . $group . '');

        $this->assign('hisiTabData', $tabData);
        $this->assign('hisiTabType', 3);
        return $this->fetch($group);
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            // 验证
            /*$result = $this->validate($data, 'SystemConfig');
            if ($result !== true) {
                return $this->error($result);
            }*/
            //奖励配置
            $top[1] = $data['top_1'];
            $top[2] = $data['top_2'];
            $top[3] = $data['top_3'];
            $top[4] = $data['top_4'];
            $top[5] = $data['top_5'];
            $mvp_prize = $data['mvp_prize'];

            unset($data['top_1']);
            unset($data['top_2']);
            unset($data['top_3']);
            unset($data['top_4']);
            unset($data['top_5']);
            unset($data['mvp_prize']);

            $data['start_time'] = strtotime($data['start_time']);
            $data['sign_time'] = strtotime($data['sign_time']);
            $data['operator'] = session('admin_user')['nick'];
            $model = RoomModel::create($data);
            if (!$model) {
                return $this->error('添加失败');
            }
            $id = $model['id'];
            //添加房间奖励配置
            foreach ($top as $k => $v) {
                if ($v) {
                    $topData = [];
                    $topData['room_id'] = $id;
                    $topData['conf_key'] = 'top_' . $k;
                    $topData['value'] = $v;
                    RoomsConfigModel::create($topData);
                }
            }
            if ($mvp_prize) {
                $mp['room_id'] = $id;
                $mp['conf_key'] = 'mvp_prize';
                $mp['value'] = $mvp_prize;
                RoomsConfigModel::create($mp);
            }
            return $this->success('添加成功', url("room/index"));
        }
        $this->assign('room', []);
        $this->assign('url', 'room/edit');
        return $this->fetch('form');
    }

    /**
     * 编辑房间
     * @param int $id
     * @return mixed|string|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit($id = 0)
    {
        $row = RoomModel::where('id', $id)->field('*')->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $id = $data['id'];
            $top[1] = $data['top_1'];
            $top[2] = $data['top_2'];
            $top[3] = $data['top_3'];
            $top[4] = $data['top_4'];
            $top[5] = $data['top_5'];
            $mvp_prize = $data['mvp_prize'];

            unset($data['top_1']);
            unset($data['top_2']);
            unset($data['top_3']);
            unset($data['top_4']);
            unset($data['top_5']);
            unset($data['mvp_prize']);

            $data['start_time'] = strtotime($data['start_time']);
            $data['sign_time'] = strtotime($data['sign_time']);
            $data['operator'] = session('admin_user')['nick'];
            $model = RoomModel::update($data, ['id' => $id]);
            if (!$model) {
                return $this->error('Update Failed!');
            }
            //更新房间奖励配置
            foreach ($top as $k => $v) {
                RoomsConfigModel::update(['value' => $v], ['room_id' => $id, 'conf_key' => 'top_' . $k]);
            }
            //mvp配置
            RoomsConfigModel::update(['value' => $mvp_prize], ['room_id' => $id, 'conf_key' => 'mvp_prize']);

            return $this->success('Success', url("room/index"));
        }
        $room_conf = RoomsConfigModel::where('room_id', $id)->select();
        foreach ($room_conf as $k => $v) {
            $row[$v['conf_key']] = $v['value'];
        }
        $this->assign('url', 'room/edit');
        $this->assign('room', $row);
        return $this->fetch('form');
    }

    /**
     * 删除配置
     * @return mixed
     * @author 橘子俊 <364666827@qq.com>
     */
    public function del()
    {
        $id = $this->request->param('id/a');
        $model = new ConfigModel();

        if ($model->del($id)) {
            return $this->success('删除成功');
        }
        // 更新配置缓存
        ConfigModel::getConfig('', true);
        return $this->error($model->getError());
    }

    public function games($group = 'base')
    {
        if ($this->request->isAjax()) {
            $where = $data = [];
            $page = $this->request->param('page/d', 1);
            $limit = $this->request->param('limit/d', 15);

            $data['data'] = RoomModel::getList($where, $page, $limit);
            $data['count'] = RoomModel::where($where)->count('id');
            $data['code'] = 0;
            return json($data);
        }
        $tabData = [];
        foreach (config('hs_system.game_group') as $key => $value) {
            $arr = [];
            $arr['title'] = $value;
            $arr['url'] = '?group=' . $key;
            $tabData['menu'][] = $arr;
        }
        $tabData['current'] = url('?group=' . $group);
        $this->assign('hisiTabData', $tabData);
        $this->assign('hisiTabType', 3);
        return $this->fetch();
    }

    public function gameResult($id = '')
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            /*// 验证
            $result = $this->validate($data, 'SystemConfig');
            if ($result !== true) {
                return $this->error($result);
            }*/
            $room_id = $data['room_id'];
            $room_data = RoomModel::where('id','=', $room_id)->find();

            //是否录入了kills
            $result_kills = RoomsPlayer::where('room_id', '=', $room_id)->where('kills', '>', 0)->select();
            if (count($result_kills) == 0) {
                return $this->error("Please input the result of kill number first");
            }
            $uid_kills = [];
            foreach ($result_kills as $rk) {
                $uid_kills[$rk['uid']] = $rk['kills'];
            }
            //取出组队信息
            $teams = RoomsTeam::where(['room_id' => $room_id])->select();
            $room_teams = [];
            foreach ($teams as $rt) {
                $room_teams[$rt['team_num']] = $rt['team_name'];
            }
            $room_players = RoomsPlayer::where(['room_id' => $room_id, 'status' => 1])->select();
            $players = [];
            foreach ($room_players as $rp) {
                $players[$rp['team_num']][] = $rp;
            }

            //比赛奖励配置
            $room_config = RoomsConfigModel::where('room_id', '=', $room_id)->select();
            $key_config = [];
            foreach ($room_config as $rc) {
                $key_config[$rc['conf_key']] = $rc['value'];
            }
            switch ($room_data['team_mode']) {
                /*case 1:
                    return $this->error("not support for solo game ");
                //1 solo
                foreach ($data as $k => $v) {
                    if (strpos($k, 'player_') !== false && $v) {
                        $id = explode('_', $k);
                        $g_result = [];
                        $g_result['player_id'] = $id[1];
                        $g_result['room_id'] = $room_id;
                        $g_result['kill_num'] = intval($v);
                        $g_
                        RoomsResult::create($g_result);
                    }
                }
                break;*/
                case 2:
                    //2 solo top x
                    //return $this->error("not support for solo game ");
                    //清除下比赛结果
                    RoomsSoloTop::where(['room_id' => $room_id])->delete();
                    foreach ($data as $k => $v) {
                        $g_result = [];
                        $g_result['prize'] = 0;
                        if (strpos($k, 'top_') !== false && $v) {
                            $rank = explode('_', $k);
                            $g_result['uid'] = intval($v);
                            $g_result['room_id'] = $room_id;
                            $g_result['rank'] = intval($rank[1]);
                            $g_result['kills'] = $uid_kills[$v] ?? 0;
                            $g_result['prize'] = 0;
                            if ($room_data['per_kill_prize'] && isset($uid_kills[$v])) {
                                $g_result['prize'] = $uid_kills[$v] * $room_data['per_kill_prize'];
                            }
                            if (isset($key_config[$k])) {
                                $g_result['prize'] += $key_config[$k];
                            }
                        }
                        if ($data['mvp'] == $v) {
                            $g_result['prize'] += $key_config['mvp'] ?? 0;
                        }
                        if (isset($g_result['uid'])) {
                            RoomsSoloTop::create($g_result);
                        }
                    }
                    break;
                //3 双人
                case 3:
                    //四人
                case 4:
                    RoomsDuoSquad::where(['room_id' => $room_id])->delete();
                    foreach ($data as $k => $v) {
                        if (strpos($k, 'top_') !== false && $v && isset($players[$v])) {
                            foreach ($players[$v] as $t) {
                                $rank = explode('_', $k);
                                $g_result = [];
                                $g_result['room_id'] = $room_id;
                                $g_result['team_num'] = $v;
                                $g_result['uid'] = $t['uid'];
                                $g_result['team_name'] = $room_teams[$v];
                                $g_result['rank'] = intval($rank[1]);
                                $g_result['prize'] = 0;
                                $g_result['kills'] = $uid_kills[$t['uid']] ?? 0;
                                if ($room_data['per_kill_prize'] && isset($uid_kills[$t['uid']])) {
                                    $g_result['prize'] = $uid_kills[$t['uid']] * $room_data['per_kill_prize'];
                                }
                                
                                if (isset($key_config[$k])) {
                                    //平分奖金
                                    $m_count = 2;
                                    if ($room_data['team_mode'] == 4) {
                                        $m_count = 4;
                                    }
                                    $g_result['prize'] += ceil($key_config[$k] / $m_count);
                                }
                                if ($data['mvp'] == $t['uid']) {
                                    $g_result['is_mvp'] = 1;
                                    $g_result['prize'] += $key_config['mvp'];
                                }
                                RoomsDuoSquad::create($g_result);
                            }
                        }
                    }
                    break;
            }
            //比赛结果图片
            RoomModel::update(['result_image' => $data['result_image'], 'result_status' => 0], ['id' => $room_id]);

            return $this->success('SUCCESS');
        }
        $room_data = RoomModel::where(['id' => $id])->find();
        $room_data['rank_player'] = 5;
        $players = RoomsPlayer::where(['room_id' => $room_data['id'], 'status' => 1])->order("team_num")->select();
        $this->assign('players', $players);
        $this->assign('room_data', $room_data);

        $result_ids = [];
        if ($room_data['team_mode'] == 2) {
            $result = RoomsSoloTop::where(['room_id' => $room_data['id']])->select();
            foreach ($result as $v) {
                $result_ids['top_' . $v['rank']] = $v['uid'];
            }
        } else if ($room_data['team_mode'] == 3 || $room_data['team_mode'] == 4) {
            $result = RoomsDuoSquad::where(['room_id' => $id])->group('rank')->field('count(id) as ct,min(team_num) as team_num,`rank`')->select();
            foreach ($result as $v) {
                $result_ids['top_' . $v['rank']] = $v['team_num'];
            }
            $mvp = RoomsDuoSquad::where(['room_id' => $room_data['id'], 'is_mvp' => 1])->find();
            if ($mvp) {
                $this->assign('mvp', $mvp['uid']);
            }
        }

        $this->assign('result_id', $result_ids);
        $team_type = 2;
        $team_num = 0;
        if ($room_data['team_mode'] == 1 || $room_data['team_mode'] == 2) {
            $team_type = 1;
        } else if ($room_data['team_mode'] == 4) {
            $team_num = 26;
        } else {
            $team_num = 51;
        }
        $this->assign('team_num', $team_num);
        $this->assign('team_type', $team_type);
        return $this->fetch('result');
    }

    /**
     * 发起结算
     * @param string $id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function passResult($id = '')
    {
        if ($id) {
            $roomInfo = RoomModel::where(['id' => $id])->find();
            if ($roomInfo["result_status"] == 1) {
                return $this->error("already settled");
            }
            $model = RoomModel::update(['result_status' => 1], ['id' => $id]);
            if ($model) {
                //发起结算
                RoomModel::Settle($roomInfo);
            }
            return $this->success("Success", url('system/room/games'), [], 1);
        }
        return $this->error("Something wrong!");
    }

    /**
     * 击杀数
     * @param string $id
     * @return string|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function kills($id = '')
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $room_id = $data['room_id'];
            $room_data = RoomModel::where(['id' => $room_id])->find();
            foreach ($data as $k => $v) {
                if (strpos($k, 'player_') !== false && $v) {
                    $kill = explode('_', $k);
                    $data = [];
                    $data['kills'] = $v;
                    $data['prize'] = $v * $room_data['per_kill_prize'];
                    RoomsPlayer::update($data, ['id' => $kill[1], 'room_id' => $room_id, 'status' => 1]);
                }
            }
            return $this->success("Success", url('system/room/games'));
        }
        $players = RoomsPlayer::where(['room_id' => $id, 'status' => 1])->order('team_num')->select();
        $this->assign('room_id', $id);
        $this->assign('players', $players);
        return $this->fetch('kills');
    }
}