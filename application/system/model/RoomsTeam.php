<?php
namespace app\system\model;

use think\Model;

class RoomsTeam extends Model{
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    protected $createTime = 'created';

    protected $updateTime = 'updated';

    protected $table = 'pg_rooms_team';

}