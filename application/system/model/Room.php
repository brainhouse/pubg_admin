<?php

namespace app\system\model;

use app\system\model\Room as RoomModel;
use hisi\Http;
use think\facade\Log;
use think\Model;

class Room extends Model
{
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    protected $createTime = 'created';

    protected $updateTime = 'updated';

    protected $table = 'pg_rooms';

    public static function getList($where, $page, $limit)
    {
        $list = self::where($where)->page($page)->limit($limit)->order('id', 'desc')->select();
        foreach ($list as $k => $v) {
            $v['start_time'] && $list[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
            $v['sign_time'] && $list[$k]['sign_time'] = date('Y-m-d H:i:s', $v['sign_time']);
            switch ($v['team_mode']) {
                case 1:
                    $list[$k]['team_mode'] = 'SOLO Per Kill';
                    break;
                case 2:
                    $list[$k]['team_mode'] = 'SOLO Top X';
                    break;
                case 3:
                    $list[$k]['team_mode'] = 'DUO';
                    break;
                case 4:
                    $list[$k]['team_mode'] = 'SQUAD';
                    break;
            }
            switch ($v['map']) {
                case 1:
                    $list[$k]['map'] = 'Sanhok';
                    break;
                case 2:
                    $list[$k]['map'] = 'Erangle';
                    break;
                case 3:
                    $list[$k]['map'] = 'Miramar';
                    break;
                case 4:
                    $list[$k]['map'] = 'Vikendi';
                    break;
            }
            switch ($v['view']) {
                case 1:
                    $list[$k]['view'] = 'TPP';
                    break;
                case 2:
                    $list[$k]['view'] = 'FPP';
                    break;
            }
            switch ($v['result_status']){
                case 0:
                    $v['result_status'] = "<font color='red'>Unsettled</font>";
                    break;
                case 1:
                    $v['result_status'] = "Settled";
                    break;
//                case 2:
//                    $v['result_status'] = "Settled";
//                    break;
            }
            switch ($v['status']){
                case 0:
                    $v['status'] = "None";
                    break;
                case 1:
                    $v['status'] = "Online";
                    break;
                case 2:
                    $v['status'] = "OnPrepare";
                    break;
            }
            $list[$k]['sign_end_time'] = $v['sign_end_time'] . ' Min';
        }
        return $list;
    }

    /**
     * 比赛结算
     * @param $room_info
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function Settle($room_info){
        switch ($room_info['team_mode']){
            case 1:
                //查询 pg_rooms_player
                $list = RoomsPlayer::where(['room_id'=>$room_info['id']])->field("id,uid,prize")->select();
                break;
            case 2:
                $list = RoomsSoloTop::where(['room_id'=>$room_info['id']])->field("id,uid,prize")->select();
                //查询 pg_rooms_solo_top
                break;
                //查询 pg_rooms_duo_squad
            case 3:
            case 4:
                $list = RoomsDuoSquad::where(['room_id'=>$room_info['id']])->field("id,team_num,uid,prize")->select();
                break;
        }
        /**
         *
        {
        "phone":18610556026,
        "uid":1039,
        "money":10,
        "room_id":12,
        "trade_bill_id":"AD15848002789449192039102497"
        }
         */
        $request = [
            'room_id'=>$room_info['id'],
            'list' => [],
        ];
        $recodeIds = [];
        foreach ($list as $v){
            $temp = [];
            $temp['uid'] = $v['uid'];
            $temp['trade_bill_id'] = "ADMST_".$room_info['team_mode'].'_'.$v['id'];
            $temp['money'] = $v['prize'];
            $request['list'][] = $temp;
            //添加结算记录
            $record['room_id'] = $room_info['id'];
            $record['team_mode'] = $room_info['team_mode'];
            $record['settle_id'] = $temp['trade_bill_id'];
            $record['uid'] = $v['uid'];
            $record['team_num'] = $v['team_num'] ?? 0;
            $record['prize'] = $v['prize'] ?? 0;
            $prizeModel = RoomsPrizeRecords::create($record);
            if ($prizeModel){
                $recodeIds[] = $prizeModel['id'];
            }
        }
        //发起http请求
        $header = [
            'Content-Type:application/json'
        ];
        $jsonData = json_encode($request);
        $res = Http::post("http://pubgNginx-753998503.ap-south-1.elb.amazonaws.com:9090/pubg/game/win", $jsonData, $header);
        if ($res && !isset($res['errno'])){
            $status = 1;
        } else {
            $status = 2;
        }
        $updateRes = RoomsPrizeRecords::where(['room_id'=>$room_info['id']])->whereIn('id', $recodeIds)->update(['status'=>$status,'desc'=>$res]);
        Log::debug("Room_Settle_Info", ['request'=>$request,'response'=>$res,'updateRes'=>$updateRes]);
    }
}