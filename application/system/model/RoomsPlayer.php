<?php

namespace app\system\model;

use think\Model;
use think\facade\Cache;

class RoomsPlayer extends Model
{
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    protected $createTime = 'created';

    protected $updateTime = 'updated';

    protected $table = 'pg_rooms_player';

    public function getList($room_id)
    {
        $list = $this->where(['room_id'=>$room_id])->select();
    }
}